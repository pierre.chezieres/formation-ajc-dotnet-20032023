﻿// See https://aka.ms/new-console-template for more information


using starwars_game;
using System.Security.AccessControl;

var menuItemList = Enum.GetValues<MenuItemType>();
void DisplayMenu()
{
    Console.WriteLine("===== Menu =====");

    foreach (var item in menuItemList)
    {
        Console.WriteLine("{0}. {1}", (int)item, (MenuItemType)item);
    }
    Console.WriteLine("================");
}

void Menu()
{
    int userChoice;
    bool quit;
    do
    {
        quit = true;
        DisplayMenu();
        Console.WriteLine("Your choice?");

        if (int.TryParse(Console.ReadLine(), out userChoice))
        {
            switch ((MenuItemType)userChoice)
            {
                case MenuItemType.NewGame:
                    {
                        NewGame();
                        break;
                    }
                case MenuItemType.LoadGame:
                    {
                        LoadGame();
                        break;
                    }
                case MenuItemType.Quit:
                    {
                        QuitGame();
                        break;
                    }
                default:
                    {
                        Console.WriteLine("Invalid input.");
                        quit = false;

                    }
                    break;
            }
        }
    }
    while (! quit);
}

void AgeValidation(int computedAge)
{
    bool askAge;
    do
    {
        Console.WriteLine("How old are you?");
        if (int.TryParse(Console.ReadLine(), out int userAge))
        {
            if (userAge > 13 & userAge == computedAge)
            {
                askAge = false;
                Console.WriteLine("Enjoy !");
            }
            else
            {
                askAge = true;
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Incorrect age.");
                Console.ForegroundColor = ConsoleColor.White;
            }
        }
        else
        {
            askAge = true;
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Invalid age input.");
            Console.ForegroundColor = ConsoleColor.White;
        }
    }while(askAge);
}

void NewGame()
{
    DateTime today = DateTime.Now;

    Console.ForegroundColor = ConsoleColor.DarkGreen;
    string title = $"A star wars game - {today:dd MMMM yyyy}".ToUpper();
    Console.WriteLine(title);

    Console.ForegroundColor = ConsoleColor.Yellow;
    string subTitle = "A copy cat of zelda".ToLower();
    Console.WriteLine(subTitle);

    Console.ForegroundColor = ConsoleColor.White;
    Console.WriteLine("Player name?");
    string? playerName = Console.ReadLine();

    Console.WriteLine("What is your birth date?");

    bool validDateInput;

    do
    {
        if (DateTime.TryParse(Console.ReadLine(), out DateTime playerBirthDate))
        {
            validDateInput = true;
            int age = (int)(today - playerBirthDate).TotalDays / 365;
            Console.WriteLine($"Is it your name ? {playerName} You are {age} years old.");
            AgeValidation(age);
        }
        else
        {
            validDateInput = false;
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Invalid date of birth.");
            Console.WriteLine("Please enter a valid birth date.");
            Console.ForegroundColor = ConsoleColor.White;
        }
    } while (validDateInput == false);
}

void LoadGame()
{
    Console.WriteLine("Game is loading...");
}

void QuitGame()
{
    Console.WriteLine("Quit...");
}

List<string> PrepEnemies()
{
    string[] enemyTab =
    {
        "Dark Vador",
        "Boba fet",
        "Empereur Palpatine"
    };

    List<string> enemyList = new List<string>()
    {
        "Dark Vador",
        "Boba fet",
        "Empereur Palpatine"
    };

    List<int> powers = new List<int>()
    {
        1, 2, 5, 10, 20
    };
    AddEnemies(enemyList);
    return enemyList;
}

void AddEnemies(List<string> enemyList)
{
    const string STOP_REQUEST = "stop";
    bool stop;
    string? input;
    do
    {
        Console.WriteLine("New enemy name?");
        input = Console.ReadLine();
        stop = input == STOP_REQUEST;
        if (input is not null)
        {
            if (!enemyList.Contains(input) && !stop)
            {
                enemyList.Add(input);
                Console.WriteLine("Enemy added in list !");
            }
        }
    } while (input != "stop");
}

void filterEnemies(List<string> enemyList)
{
    var query = from enemy in enemyList
                let eneMaj = enemy[0].ToString().ToUpper() + enemy.Substring(1).ToLower()
                let premierCar = enemy[0]
                let monObjet = new { Maj = eneMaj, Initial = enemy }
                where enemy.StartsWith("D") || enemy.EndsWith("D")
                orderby enemy descending
                select monObjet;
    foreach (var enemy in query)
    {
        Console.WriteLine(enemy.Maj);
        Console.WriteLine(enemy.Initial);
    }
}


void displayAllEnemies(List<string> enemyList)
{
    foreach (var enemy in enemyList)
    {
        Console.WriteLine(enemy);
    }
}


Menu();

List<string> enemyList = PrepEnemies();

filterEnemies(enemyList);

displayAllEnemies(enemyList);





