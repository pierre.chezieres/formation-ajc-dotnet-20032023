﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace starwars_game
{
    /// <summary>
    /// Menu items
    /// </summary>
    public enum MenuItemType
    {
       NewGame = 1,
       LoadGame = 2,
       Quit = 3
    }
}
