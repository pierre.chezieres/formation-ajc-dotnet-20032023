﻿using System.ComponentModel.Design;
using System.Numerics;
using game.api;
using menu.api;

Menu menu = new(Console.WriteLine, Console.ReadLine);
MenuItem nouvellePartie = new(1, "Nouvelle Partie", 1);
MenuItem quitter = new(2, "Quitter", 2);
menu.Ajouter(nouvellePartie);
menu.Ajouter(quitter);
bool continuer = true;
Dictionary<int, Action> actionsMenu = new()
{
    { 1, () =>
        {
            PersoPrincipal hero = new PersoPrincipal("Yoda", Console.WriteLine, Console.ReadLine);
            hero.blessureMortelle += (hero) => Console.WriteLine($"{hero} est mort. GAME OVER !!!!");
            Ennemi ennemi = new Ennemi("Droid");
            ennemi.blessureMortelle += (ennemi) => Console.WriteLine($"{ennemi} est mort.");
            Game game = new(1, DateTime.Now, hero);
            game.debutPartie += (gameId, debut, perso) => 
                                Console.WriteLine($"La partie {gameId} avec {perso} commence ({debut})");
            game.Demarrer();
            int round = 1;
            Console.Clear();
            while (ennemi.EstVivant && hero.EstVivant)
            {
                Console.WriteLine("================================");
                Console.WriteLine(round);
                Console.WriteLine($"{hero} attaque {ennemi}");
                
                hero.Attaquer(ennemi);
                if(ennemi.EstVivant) {
                    Console.WriteLine($"{ennemi} attaque {hero}");
                    ennemi.Attaquer(hero);
                }
                round++;
            }
        }
    },
    {2, () => {continuer = false;} }
};
do
{
    menu.Afficher();
    if (int.TryParse(Console.ReadLine(), out int input))
        actionsMenu[input]();
} while (continuer);